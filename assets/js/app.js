import Vue from 'vue';
import Example from './components/Example'
import Tabsn from './components/Tabsn'
import Carouseln from './components/Carouseln'
import Cartbutton from './components/Cartbutton'


require('../css/app.css');
/**
 * Create a fresh Vue Application instance
 */
new Vue({
    el: '#app',
    components: {Example, Tabsn, Carouseln, Cartbutton}
});
