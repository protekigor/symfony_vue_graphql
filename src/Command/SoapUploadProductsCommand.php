<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\Soap\Component\Client as SoapClient;
use App\Document\SoapConnection;
use App\Document\SoapProducts;
use Doctrine\ODM\MongoDB\DocumentManager;

class SoapUploadProductsCommand extends Command
{
    /**
     * @var SoapClient
     */
    private $soap;
    private $dm;

    protected static $defaultName = 'soap:upload-products';

    protected function configure()
    {
        $this
            ->setDescription('Загрузка каталога из Soap')
        ;
    }

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $executionStartTime = microtime(true);
        $this->soap = new SoapClient('admin', 'admin');

        $this->getItems();

        $executionEndTime = microtime(true);
        $seconds = $executionEndTime - $executionStartTime;
        $output->writeln("This script took $seconds to execute.");

        return 0;
    }

    public function getItems()
    {
        $user = $this->dm->find(SoapConnection::class, '5e183b12d9f394304c691c82');

        $batchSize = 20;
        $sessionId = $this->soap->openSession(
            [
                'theLogin' => $user->getLogin(),
                'thePass' => $user->getPass()
            ]
        );

        $soapProducts = $this->soap->obtainEsEima(
            [
                'eima-app-' => [$sessionId->server, '/', 'eima-app.protek.ru'],
                'theSessionId' => $sessionId->statusString,
                'theEZakazXML' => [],
                'theRowTs' => $user->getTheRowTs(),
                'theRowCount' => 10000
            ]
        );

        if(isset($soapProducts->ezakazXML->item)) {
            $lastRowTs = end($soapProducts->ezakazXML->item);
            $user->setTheRowTs($lastRowTs->rowTs);
            $this->dm->flush();
        }

        if($soapProducts->ezakazXML->totalRowCount > 0) {

            foreach ($soapProducts->ezakazXML->item as $i => $item) {
                $soapProduct = new SoapProducts();
                $soapProduct->setActiveFlag($item->activeFlag);
                $soapProduct->setCvpItemId((isset($item->cvpItemId)) ? $item->cvpItemId : null);
                $soapProduct->setGuidEsId($item->guidEsId);
                $soapProduct->setGuidInstruction($item->guidInstruction);
                $soapProduct->setGuidProducer($item->guidProducer);
                $soapProduct->setItemName($item->itemName);
                $soapProduct->setPrNotRecept(isset($item->prNotRecept) ? $item->prNotRecept : null);
                $soapProduct->setProducerName(isset($item->producerName) ? $item->producerName : null);

                $this->dm->persist($soapProduct);
                if (($i % $batchSize) == 0) {
                    $this->dm->flush();
                    $this->dm->clear();
                }
            }

            $this->dm->flush();
            $this->dm->clear();

            $this->getItems();
        }
    }
}
