<?php

namespace App\Service\Mindbox\Component;

use App\Component\Request as BaseRequest;
use App\Service\Mindbox\Exceptions\MindboxApiException;
use App\Service\Mindbox\Model\Error;

/**
 * Class Request
 *
 * @package App\Service\Mindbox\Component
 */
class Request extends BaseRequest
{
    const V2 = 'v2';
    const V3 = 'v3';

    private $url = 'https://api.mindbox.ru/v3/operations/';

    /**
     * Request constructor.
     *
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
        $this->setException(MindboxApiException::class);
    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return array
     */
    public function __call(string $method, array $arguments): array
    {
        parent::addHeaders(['Mindbox secretKey=' . $this->key]);
        parent::setUrl(
            sprintf(
                '%s/%s',
                $this->url,
                $arguments[0]['action']
            )
        );
        return call_user_func_array(
            [$this, 'makeRequest'],
            [strtoupper($method), $arguments[0]['data'], ['validationMessages', 'errorMessage']]
        );
    }

    public function getErrorMessage($result, array $errorIndexes): bool
    {
        $error = '';

        foreach ($errorIndexes as $index) {
            if (!empty($result['result'][$index])) {
                if (!empty($result['result'][$index] && is_array($result['result'][$index]))) {
                    foreach ($result['result'][$index] as $field) {
                        if (!empty($field['message'])) {
                            $error .= sprintf(
                                "[%s] | ",
                                $field['message']
                            );
                        }
                    }
                } else {
                    if (!empty($result['result'][$index])) {
                        $error .= sprintf(
                            "%s | ",
                            $result[$index]['title']
                        );
                    }
                }

                $error = trim($error, ' | ');
            }
        }

        if (!empty($error)) {
            Error::set('lastError', $error);
        }

        return true;
    }
}

