<?php

namespace App\Service\Mindbox\Exceptions;

/**
 * Class MindboxException
 *
 * @package App\Service\Mindbox\Exceptions
 */
class MindboxException extends \Exception
{
}
