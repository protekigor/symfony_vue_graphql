<?php

namespace App\Service\Soap\Exceptions;

/**
 * Class SoapException
 *
 * @package App\Service\Soap\Exceptions
 */
class SoapException extends \Exception
{
}
