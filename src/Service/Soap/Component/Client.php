<?php

namespace App\Service\Soap\Component;

use App\Component\Proxy;

/**
 * Class Client
 *
 * @package App\Service\Soap\Component
 */
class Client
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Client constructor.
     *
     * @param string $apiLogin
     * @param string $apiPass
     *
     * @return self
     * @throws \Exception
     */
    public function __construct(
        string $apiLogin,
        string $apiPass
    ) {
        $this->request = new Proxy(Request::class, [$apiLogin, $apiPass]);

        return $this;
    }

    /**
     * @param string    $method
     * @param array     $options
     *
     * @return array|object
     * @throws \Exception
     */
    public function request(string $method, array $options = [])
    {
        return $this->request->{$method}($options);
    }

    /**
     * @param array $options
     *
     * @return array|object
     * @throws \Exception
     */
    public function openSession(array $options = [])
    {
        return $this->request(__FUNCTION__, $options);
    }

    /**
     * @param array $options
     *
     * @return array|object
     * @throws \Exception
     */
    public function getOrderStatus(array $options = [])
    {
        return $this->request(__FUNCTION__, $options);
    }

    /**
     * @param array $options
     *
     * @return array|object
     * @throws \Exception
     */
    public function obtainEsEima(array $options = [])
    {
        return $this->request(__FUNCTION__, $options);
    }
}
