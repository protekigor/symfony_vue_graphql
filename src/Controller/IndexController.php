<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use App\Component\Logger;

class IndexController extends Controller
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
//        Logger::getInstance()->log(
//            'soap_callback',
//            sprintf(
//                '%s',
//                var_export('1', true)
//            )
//        );
        return $this->render('index/index.html.twig');
    }
}
