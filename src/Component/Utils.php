<?php

namespace App\Component;

/**
 * Class Utils
 *
 * @package Component
 */
class Utils
{
    /**
     * @param array $arr
     * @param bool  $trimming
     *
     * @return array
     */
    public static function clearArray(array $arr, bool $trimming = true): array
    {
        if (!is_array($arr)) {
            return $arr;
        }

        $result = [];

        foreach ($arr as $index => $node) {
            $result[ $index ] = (is_array($node))
                ? self::clearArray($node, $trimming)
                : ($trimming ? trim($node) : $node);

            if ($result[ $index ] === ''
                || $result[ $index ] === null
                || count($result[ $index ]) < 1
            ) {
                unset($result[ $index ]);
            }
        }

        return $result;
    }

    /**
     * @param array $array
     *
     * @return string
     */
    public static function arrayToString(array $array): string
    {
        $string = '';

        foreach ($array as $index => $value) {
            $string .= sprintf(
                '[%s => %s]',
                $index,
                (is_array($value) ? self::arrayToString($value) : $value)
            );
        }

        unset($index, $value);

        return $string;
    }

    /**
     * @param array $array
     *
     * @return bool
     */
    public static function isAssoc(array $array): bool
    {
        $keys = array_keys($array);

        return array_keys($keys) !== $keys;
    }
}
