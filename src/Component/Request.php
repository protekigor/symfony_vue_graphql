<?php

namespace App\Component;

use App\Exception\InvalidRequestMethodException;
use App\Exception\CurlException;
use App\Exception\InvalidJsonException;
use App\Component\Utils;
use App\Component\Logger;

/**
 * Class Request
 *
 * @package App\Component
 */
class Request
{
    const METHODS = ['GET', 'POST'];

    /**
     * @var string
     */
    private $exceptionClass;

    /**
     * @var string
     */
    private $url;

    /**
     * @var bool
     */
    private $isBasicAuth = false;

    /**
     * @var string
     */
    private $apiLogin;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var array
     */
    private $headers = ['Content-Type: application/json; charset=utf-8'];

    public function resetHeaders()
    {
        $this->headers = ['Content-Type: application/json; charset=utf-8'];
    }

    /**
    * @param array $setHeaders
    */
    public function setHeaders(array $setHeaders)
    {
        $this->headers = $setHeaders;
    }

    /**
    * @param array $addHeaders
    */
    public function addHeaders(array $addHeaders)
    {
        $this->headers = array_merge($this->headers, $addHeaders);
    }

    public function removeHeaders()
    {
        $this->headers = [];
    }

    /**
     * Request constructor.
     *
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @param string $class
     */
    public function setException(string $class)
    {
        $this->exceptionClass = $class;
    }

    /**
     * @param string $login
     * @param string $password
     */
    protected function setBasicAuth(string $login, string $password)
    {
        $this->isBasicAuth = true;
        $this->apiLogin = $login;
        $this->apiKey = $password;
    }

    /**
     * @param string $type
     * @param array  $params
     * @param array  $errors
     *
     * @return array
     * @throws \CoreBundle\Exception\CurlException
     * @throws \CoreBundle\Exception\InvalidJsonException
     * @throws \CoreBundle\Exception\InvalidRequestMethodException
     */
    protected function makeRequest(string $type, array $params = [], array $errors = []): array
    {
        if (!in_array($type, self::METHODS)) {
            throw new InvalidRequestMethodException(
                sprintf(
                    "Invalid request method '%s'",
                    $type
                )
            );
        }
        if ($type == 'GET') {
            if (!is_null(parse_url($this->url, PHP_URL_QUERY))) {
                $this->url .= '&';
            } else {
                if(!empty($params)) {
                    $this->url .= '?';
                }
            }

            $this->url .= http_build_query($params);
        }

        $curlHandler = curl_init();
        curl_setopt($curlHandler, CURLOPT_URL, $this->url);
        curl_setopt($curlHandler, CURLOPT_FAILONERROR, false);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandler, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandler, CURLOPT_TIMEOUT, 300);
        curl_setopt($curlHandler, CURLOPT_CONNECTTIMEOUT, 300);

        $this->basicAuth($curlHandler);

        if ($type == 'POST') {
            if (!empty($params)) {
                curl_setopt($curlHandler, CURLOPT_POSTFIELDS, json_encode($params));
            } else {
                $this->headers[] = 'Content-Length: 0';
            }

            curl_setopt($curlHandler, CURLOPT_POST, true);
        }

        if (!empty($this->headers)) {
            curl_setopt(
                $curlHandler,
                CURLOPT_HTTPHEADER,
                $this->headers
            );
        }

        $responseBody = curl_exec($curlHandler);
        $statusCode = curl_getinfo($curlHandler, CURLINFO_HTTP_CODE);
        $contentType = curl_getinfo($curlHandler, CURLINFO_CONTENT_TYPE);

        $errno = curl_errno($curlHandler);
        $error = curl_error($curlHandler);
        curl_close($curlHandler);
        $this->resetHeaders();

        if ($statusCode >= 500) {
            throw new $this->exceptionClass(
                sprintf(
                    "[Error type: 500] -> [Request method: %s | url: %s | data: %s] -> Response: %s]",
                    $type,
                    $this->url,
                    (
                        ($type == 'POST' && !empty($params)) ?
                        json_encode($params) :
                        'empty-data'
                    ),
                    $responseBody
                )
            );
        }

        if ($contentType == "text/html; charset=utf-8" || $contentType == "application/octet-stream" || $contentType == "text/html; charset=us-ascii") {
            if ($statusCode >= 400) {
                throw new $this->exceptionClass($error);
            }

            $result['data'] = $responseBody;

            return $result;
        }

        $result = json_decode($responseBody, true);
        $resultErrors = '';

        if (JSON_ERROR_NONE !== ($jsonError = json_last_error())) {
            throw new InvalidJsonException(
                "Invalid JSON in the API response body. Error code #$jsonError",
                $jsonError
            );
        } else {
            $this->getErrorMessage($result, $errors);

            foreach ($errors as $index) {
                if (!empty($result[$index])) {
                    $resultErrors .= sprintf(
                        "%s: \"%s\" | ",
                        $index,
                        (is_array($result[$index]) ?
                            Utils::arrayToString($result[$index]) :
                            $result[$index]
                        )
                    );
                }
            }

            $resultErrors = trim($resultErrors, ' | ');
            unset($index);
        }

        if (!empty($resultErrors)) {
            throw new $this->exceptionClass(
                sprintf(
                    "[Error type: 500] -> [Error: '%s' | Request method: %s | url: %s | data: %s] -> Response: %s]",
                    $resultErrors,
                    $type,
                    $this->url,
                    (
                        ($type == 'POST' && !empty($params)) ?
                        json_encode($params) :
                        'empty-data'
                    ),
                    $responseBody
                )
            );
        }

        if ($statusCode >= 400) {
            throw new $this->exceptionClass(
                (!empty($resultErrors) ? $resultErrors : json_encode($result))
            );
        }

//        if ($errno && in_array($errno, array(6, 7, 28, 34, 35)) && $this->retry < 3) {
//            $errno = null;
//            $error = null;
//            $this->retry += 1;
//            $this->makeRequest(
//                $url,
//                $method,
//                $parameters
//            );
//        }

        if ($errno) {
            throw new CurlException($error, $errno);
        }

        return $result;
    }

    /**
     * @param resource $curlHandler
     */
    private function basicAuth(&$curlHandler)
    {
        if ($this->isBasicAuth) {
            if (empty($this->apiLogin) || empty($this->apiKey)) {
                throw new $this->exceptionClass('Need basic auth!');
            }

            curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curlHandler, CURLOPT_USERPWD, sprintf('%s:%s', $this->apiLogin, $this->apiKey));
        }
    }

    public function getErrorMessage($result, array $errorIndexes)
    {
        return true;
    }
}
