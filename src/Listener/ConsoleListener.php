<?php

namespace App\Listener;

use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Event\ConsoleEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Listener\Traits\ListenerTrait;
use App\Component\Logger;

/**
 * Class ConsoleListener
 *
 * @package App\Listener
 */
class ConsoleListener
{
    use ListenerTrait;

    /* @var ContainerInterface $container */
    public $container;

    private static $bundles = [];

    /**
     * @throws \Exception
     */
    public function onStart(ConsoleEvent $event)
    {
        Logger::getInstance($this->container);
    }

    /**
     * @param \Symfony\Component\Console\Event\ConsoleErrorEvent $event
     *
     * @throws \Exception
     */
    public function onError(ConsoleErrorEvent $event)
    {
        if ($this->container->get('kernel')->getEnvironment() === 'prod') {
            Logger::getInstance($this->container);
            $this->getFullStringErrorMessage($event);
            $output = $event->getOutput();
            $output->writeln("<error>Oops.. Something wrong!</error>");
            exit(1);
        }
    }
}
