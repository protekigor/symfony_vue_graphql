<?php

namespace App\Exception;

use Exception\Traits\ExceptionTrait;

/**
 * Class MonologGetLevelException
 *
 * @package Exception
 */
class MonologGetLevelException extends \Exception
{
    use ExceptionTrait;
}
