<?php

namespace App\Exception\Traits;

/**
 * Trait ExceptionTrait
 *
 * @package App\Exception\Traits
 */
trait ExceptionTrait
{
    /**
     * @return string
     */
    public function getTotalInfo(): string
    {
        $traces = '';
        $startWriteTrace = false;
        $allTraces = array_reverse($this->getTrace());

        foreach ($allTraces as $key => $trace) {
            if ($trace['function'] == 'execute') {
                $startWriteTrace = true;
            }

            if ($startWriteTrace) {
                $path = $trace['class'] ?? $trace['file'];
                $nextTrace = $allTraces[$key + 1] ?? $trace;

                $line = $nextTrace['line'] ??
                    (
                    !empty($trace['line']) ?
                        "({$trace['line']})" :
                        $this->getLine()
                    );

                $traceClass = !empty($trace['class']) ?
                    preg_replace("#^.+\\\([^\\\]+)$#", "$1", $path) :
                    preg_replace("#^.+/([^/]+)$#", "$1", $path);

                if ($nextTrace == $trace) {
                    $traces .= "\"{$trace['function']}\"";
                } else {
                    $traces .= "\"{$trace['function']}\" in {$traceClass}({$line}) -> ";
                }
            }
        }

        return sprintf(
            "[%s][line = %d][message \"%s\"][trace: %s]",
            preg_replace("#^.+/([^/]+)$#", "$1", $this->getFile()),
            $this->getLine(),
            $this->getMessage(),
            trim($traces, ' -> ')
        );
    }
}