<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Connection
 *
 * @MongoDB\Document
 */
class SoapConnection
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $login;

    /**
     * @MongoDB\Field(type="string")
     */
    private $pass;

    /**
     * @MongoDB\Field(type="integer")
     */
    private $theRowTs;

    /**
     * @MongoDB\Field(type="integer")
     */
    private $theRowTsForTest;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return Connection
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set pass
     *
     * @param string $pass
     *
     * @return Connection
     */
    public function setPass($pass)
    {
        $this->pass = $pass;

        return $this;
    }

    /**
     * Get pass
     *
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * Set theRowTs
     *
     * @param integer $theRowTs
     *
     * @return Connection
     */
    public function setTheRowTs($theRowTs)
    {
        $this->theRowTs = $theRowTs;

        return $this;
    }

    /**
     * Get theRowTs
     *
     * @return int
     */
    public function getTheRowTs()
    {
        return $this->theRowTs;
    }

    /**
     * Set theRowTsForTest
     *
     * @param integer $theRowTsForTest
     *
     * @return Connection
     */
    public function setTheRowTsForTest($theRowTsForTest)
    {
        $this->theRowTsForTest = $theRowTsForTest;

        return $this;
    }

    /**
     * Get theRowTsForTest
     *
     * @return int
     */
    public function getTheRowTsForTest()
    {
        return $this->theRowTsForTest;
    }
}

