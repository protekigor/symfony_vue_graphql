<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * SoapProducts
 *
 * @MongoDB\Document
 */
class SoapProducts
{
    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="boolean")
     */
    private $activeFlag;

    /**
     * @MongoDB\Field(type="integer")
     */
    private $cvpItemId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $guidEsId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $guidInstruction;

    /**
     * @MongoDB\Field(type="string")
     */
    private $guidProducer;

    /**
     * @MongoDB\Field(type="string")
     */
    private $itemName;

    /**
     * @MongoDB\Field(type="string")
     */
    private $producerName;

    /**
     * @MongoDB\Field(type="boolean")
     */
    private $prNotRecept;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activeFlag
     *
     * @param boolean $activeFlag
     *
     * @return SoapProducts
     */
    public function setActiveFlag($activeFlag)
    {
        $this->activeFlag = $activeFlag;

        return $this;
    }

    /**
     * Get activeFlag
     *
     * @return bool
     */
    public function getActiveFlag()
    {
        return $this->activeFlag;
    }

    /**
     * Set cvpItemId
     *
     * @param integer $cvpItemId
     *
     * @return SoapProducts
     */
    public function setCvpItemId($cvpItemId)
    {
        $this->cvpItemId = $cvpItemId;

        return $this;
    }

    /**
     * Get cvpItemId
     *
     * @return int
     */
    public function getCvpItemId()
    {
        return $this->cvpItemId;
    }

    /**
     * Set guidEsId
     *
     * @param string $guidEsId
     *
     * @return SoapProducts
     */
    public function setGuidEsId($guidEsId)
    {
        $this->guidEsId = $guidEsId;

        return $this;
    }

    /**
     * Get guidEsId
     *
     * @return string
     */
    public function getGuidEsId()
    {
        return $this->guidEsId;
    }

    /**
     * Set guidInstruction
     *
     * @param string $guidInstruction
     *
     * @return SoapProducts
     */
    public function setGuidInstruction($guidInstruction)
    {
        $this->guidInstruction = $guidInstruction;

        return $this;
    }

    /**
     * Get guidInstruction
     *
     * @return string
     */
    public function getGuidInstruction()
    {
        return $this->guidInstruction;
    }

    /**
     * Set guidProducer
     *
     * @param string $guidProducer
     *
     * @return SoapProducts
     */
    public function setGuidProducer($guidProducer)
    {
        $this->guidProducer = $guidProducer;

        return $this;
    }

    /**
     * Get guidProducer
     *
     * @return string
     */
    public function getGuidProducer()
    {
        return $this->guidProducer;
    }

    /**
     * Set itemName
     *
     * @param string $itemName
     *
     * @return SoapProducts
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }

    /**
     * Get itemName
     *
     * @return string
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * Set producerName
     *
     * @param string $producerName
     *
     * @return SoapProducts
     */
    public function setProducerName($producerName)
    {
        $this->producerName = $producerName;

        return $this;
    }

    /**
     * Get producerName
     *
     * @return string
     */
    public function getProducerName()
    {
        return $this->producerName;
    }

    /**
     * @param bool $prNotRecept
     */
    public function setPrNotRecept($prNotRecept) : void
    {
        $this->prNotRecept = $prNotRecept;

    }

    /**
     * @return bool
     */
    public function getPrNotRecept(): bool
    {
        return $this->prNotRecept;
    }

}

