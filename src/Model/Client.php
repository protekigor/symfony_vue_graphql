<?php

namespace App\Model;

use App\Model\Traits\ModelProvider;
use App\Model\Model as ModelInterface;

/**
 * Class Client
 *
 * @package Model
 */
class Client implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];
}