<?php

namespace App\Model;

use App\Model\Traits\ModelProvider;
use App\Model\Model as ModelInterface;
use App\Component\Utils;

/**
 * Class Options
 *
 * @package Model
 */
class Options implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];

    /**
     * Проверяет, есть ли значение в справочнике
     *
     * @param mixed $value
     * @return bool
     */
    final public static function issetValue($value)
    {
        return in_array($value, self::$data);
    }

    /**
     * Добавляет в справочник набор параметров
     *
     * @param array $options
     * @throws \InvalidArgumentException
     * @return void
     */
    final public static function configure(array $options)
    {
        if (Utils::isAssoc($options)) {
            throw new \InvalidArgumentException('The configuration array must be associative!');
        }
        self::$data = array_merge(self::$data, $options);
    }
}
