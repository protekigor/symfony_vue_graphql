<?php

namespace App\Model\Traits;

trait ModelProvider
{
    /**
     * {@inheritDoc}
     */
    public static function set(string $key, $value)
    {
        self::$data[$key] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public static function get(string $key)
    {
        return self::$data[$key] ?? null;
    }

    /**
     * {@inheritDoc}
     */
    public static function removeValue(string $key)
    {
        if (array_key_exists($key, self::$data)) {
            unset(self::$data[$key]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function getCount()
    {
        return count(self::$data);
    }
}
