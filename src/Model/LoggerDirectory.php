<?php

namespace App\Model;

use App\Model\Traits\ModelProvider;
use App\Model\Model as ModelInterface;

class LoggerDirectory implements ModelInterface
{
    use ModelProvider;

    /**
     * @var mixed
     */
    protected static $data = [];
}